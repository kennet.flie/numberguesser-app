package com.example.NumberGuesserapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Speicher s = Speicher.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int maxRange;
        Button b = findViewById(R.id.start);
        TextView textView = findViewById(R.id.textView);
        EditText rangegiver = findViewById(R.id.ranger);

        Button tt = findViewById(R.id.tutorialbutton);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                s.setCards(NumbGuessBackbone.GenCards(Integer.parseInt(rangegiver.getText().toString())));
                Intent i = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(i);
            }
        });

        tt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Tutorial.class);
                startActivity(i);
            }

        });



    }
}