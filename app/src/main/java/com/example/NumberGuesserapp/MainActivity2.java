package com.example.NumberGuesserapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {
    int rows = 0;
    int[] is1;
    EditText rangegiver = findViewById(R.id.ranger);

    Speicher s = Speicher.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        LinearLayout lv = findViewById(R.id.listView);
        Button guess = findViewById(R.id.guess);

        List<Integer> ids = new ArrayList<>();
        int[][] array = s.getCards();
        is1 = new int[s.getCards().length];

        for (int[] w : array) {
            int row = rows;
            rows++;
            TextView a = new TextView (this);
            a.setText(Arrays.toString(w));
            a.setTextSize(30);
            is1[row] = 0;

            a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(is1[row] == row + 1) {
                        a.setTextColor(getResources().getColor(android.R.color.background_light));
                        is1[row] = 0;
                    }
                    else {
                        a.setTextColor(getResources().getColor(android.R.color.holo_green_light));
                        is1[row] = row + 1;
                    }
                }
            });
            lv.addView(a);
        };


        guess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ids.clear();
                for (int num : is1) {
                    if(num != 0) {
                        ids.add(num);
                    }
                }
                int[] idList = new int[ids.size()];
                for (int i = 0; i < ids.size(); i++) {
                    idList[i] = ids.get(i);
                }
                int guessout = NumbGuessBackbone.CalcGuess(Integer.parseInt(rangegiver.getText().toString()), idList);
                if (guessout == -1) {};
                guess.setText(Integer.toString(guessout));
                //guess.setText(Arrays.toString(idList));



                //Intent i = new Intent(MainActivity2.this, GuessScreen.class);
                //startActivity(i);
            }
        });


    };
};